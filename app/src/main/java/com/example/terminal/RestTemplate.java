package com.example.terminal;

import com.example.terminal.models.LoginRequest;
import com.example.terminal.models.LoginResponse;
import com.example.terminal.models.PayResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RestTemplate {
    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://terminal.zingpay.ru/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private final ServerApi serverApi = retrofit.create(ServerApi.class);


    public Call<JsonObject> createTransaction(String token, Long amount) {
        return serverApi.createTransaction(token,
                amount);
    }

    public Call<LoginResponse> login(LoginRequest loginRequest) {
        return serverApi.login(loginRequest);
    }


    Call<PayResponse> receivePay(String token,String payRequest) {
        return serverApi.receivePay(token, payRequest);
    }

    Call<JsonObject> checkTransaction(String token, String transactionId) {
        return serverApi.checkTransaction(token, transactionId);
    }

    Call<JsonObject> declineTransaction(String token, String transactionId) {
        return serverApi.declineTransaction(token, transactionId);
    }
}

