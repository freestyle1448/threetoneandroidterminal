package com.example.terminal;

import com.example.terminal.models.LoginRequest;
import com.example.terminal.models.LoginResponse;
import com.example.terminal.models.PayResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

interface ServerApi {
    @POST("/api/terminal/auth")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @FormUrlEncoded
    @POST("api/terminal/transaction/create")
    Call<JsonObject> createTransaction(@Header("Token") String userToken,@Field(value = "amount") Long amount);

    @GET("api/terminal/transaction/{id}/check")
    Call<JsonObject> checkTransaction(@Header("Token") String userToken,@Path("id") String transactionId);

    @FormUrlEncoded
    @POST("api/terminal/transaction/ultrasonic/pay")
    Call<PayResponse> receivePay(@Header("Token") String userToken, @Field(value = "senderId") String payRequest);

    @POST("api/terminal/transaction/{id}/cancel")
    Call<JsonObject> declineTransaction(@Header("Token") String userToken,@Path("id") String transactionId);
}
