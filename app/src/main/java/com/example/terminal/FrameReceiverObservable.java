package com.example.terminal;


import android.content.Context;

import org.quietmodem.Quiet.FrameReceiver;
import org.quietmodem.Quiet.FrameReceiverConfig;

import java.net.SocketTimeoutException;

import rx.Observable;
import rx.Subscription;

class FrameReceiverObservable {
    private static final String PROFILE = "ultrasonic-experimental";
    private static final int TIMEOUT = 30;
    private static final int BUFFER_SIZE = 1024;

    static Observable<byte[]> create(Context context) {
        return Observable.create(subscriber -> {
            try {
                FrameReceiverConfig receiverConfig = new FrameReceiverConfig(context, PROFILE);
                FrameReceiver frameReceiver = new FrameReceiver(receiverConfig);
                frameReceiver.setBlocking(TIMEOUT, 0);
                final byte[] buf = new byte[BUFFER_SIZE];

                subscriber.add(new Subscription() {
                    boolean isUnsubscribe;

                    @Override
                    public void unsubscribe() {
                        frameReceiver.close();
                        isUnsubscribe = true;
                    }

                    @Override
                    public boolean isUnsubscribed() {
                        return isUnsubscribe;
                    }
                });

                while (!subscriber.isUnsubscribed()) {
                    long recvLen;
                    try {
                        recvLen = frameReceiver.receive(buf);
                        byte[] immutableBuf = java.util.Arrays.copyOf(buf, (int) recvLen);
                        subscriber.onNext(immutableBuf);
                    } catch (SocketTimeoutException e) {
                        // ignore timeouts - attempt new receive
                    }
                }

            } catch (Exception e) {
                subscriber.onError(e);
            }

        });
    }
}
