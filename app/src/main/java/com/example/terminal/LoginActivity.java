package com.example.terminal;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.terminal.models.LoginRequest;
import com.example.terminal.models.LoginResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.terminal.models.Constants.APP_PREFERENCES;
import static com.example.terminal.models.Constants.TERMINAL_LOGIN;
import static com.example.terminal.models.Constants.TERMINAL_TOKEN;

public class LoginActivity extends AppCompatActivity {
    private final RestTemplate serverApi = new RestTemplate();
    private SharedPreferences mSettings;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    @BindView(R.id.loginEditText)
    EditText loginET;
    @BindView(R.id.passwordEditText)
    EditText passwordET;
    @BindView(R.id.loginButton)
    Button loginButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        /*try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setTheme(R.style.AppTheme);*/ //TODO сплэш
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        initView();
    }
    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
    }
    private void getAuthorizedUser() {
        final String userToken = mSettings.getString(TERMINAL_TOKEN, "");
        if (!userToken.isEmpty()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void initView() {
        ButterKnife.bind(this);

        getSupportActionBar().setElevation(0);
        requestPermission();
        getAuthorizedUser();


    }

    @OnEditorAction(R.id.passwordEditText)
    public boolean submitLogin(int actionId) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_SEND) {
            onLogin();
            handled = true;
        }
        return handled;
    }

    @OnClick(R.id.loginButton)
    public void onLogin() {
        if (TextUtils.isEmpty(loginET.getText())) {
            loginET.requestFocus();
            loginET.setError("Введите логин!");

            return;
        }
        if (TextUtils.isEmpty(passwordET.getText())) {
            passwordET.requestFocus();
            passwordET.setError("Введите пароль!");

            return;
        }

        serverApi.login( LoginRequest.builder()
                .username(loginET.getText().toString())
                .password(passwordET.getText().toString())
                .build())
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.body() != null) {
                            final String token = response.body().getToken();
                            SharedPreferences.Editor editor = mSettings.edit();
                            editor.putString(TERMINAL_TOKEN, token);
                            editor.putString(TERMINAL_LOGIN, response.body().getUser().getUsername());
                            editor.apply();

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            LoginActivity.this.finish();
                            runOnUiThread(() -> {
                                loginET.getText().clear();
                                passwordET.getText().clear();
                            });
                        }else {
                            Toast.makeText(LoginActivity.this, "Произошла ошибка, повторите запрос позже.\nОшибка - "
                                    + "Пользователь с указанными данными не найден!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Log.e("requestError", String.valueOf(t.getLocalizedMessage()));
                        Toast.makeText(LoginActivity.this, "Произошла ошибка, повторите запрос позже.\nОшибка - "
                                + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }
}
