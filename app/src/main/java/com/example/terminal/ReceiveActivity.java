package com.example.terminal;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.terminal.models.PayResponse;
import com.google.gson.JsonObject;
import com.google.zxing.WriterException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import io.chirp.chirpsdk.ChirpSDK;
import io.chirp.chirpsdk.interfaces.ChirpEventListener;
import io.chirp.chirpsdk.models.ChirpError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

import static com.example.terminal.models.Constants.APP_PREFERENCES;
import static com.example.terminal.models.Constants.TERMINAL_TOKEN;

public class ReceiveActivity extends AppCompatActivity {
    private String userToken;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private final RestTemplate restTemplate = new RestTemplate();
    private Subscription frameSubscription = Subscriptions.empty();
    private TextView doneText;
    private ImageView iconDone;
    private Button doneButton;
    private Button buttonInit;
    private ImageView qrView;
    private Switch sound;
    private TextView dtmf;
    private TextView ultrasonic;
    private TextView payCodeTV;
    private TextView payCodeTX;
    private boolean isCheckSend = false;
    private ProgressBar progressBar;
    SharedPreferences sharedPreferences;

    final Timer timer = new Timer();
    String CHIRP_APP_KEY = "E2444f90d89AdDD4E43c939aC";
    String CHIRP_APP_SECRET = "F6Ceb2aD48a040eDACF3131d36D2dBBdB0A59eF1A611dF800B";
    String CHIRP_APP_CONFIG = "cx2kkmP0ZKAxFSRpq4PYnLP9pkmsU4ltGnephl50ucVl+4p7GWZ5DM/goFHBwPI+bgJtg14ZAZPK4Cj4uhwWutf+B6z+HtnIKjZnQ4WHi+VFsE4a7DVSnNPuX2kNz1fjM74LNbFOaaeE+9k0A5Gv2xy+ZBf45LF0gSuFHXhHChbateBOmbiXOyNxJtHV9VoVZFcIYipVEmPJmETw5DMagE9Le+HyHsltBZufKZwXmpMJX1ir70TVGA/Tw6TnxNnwbnc935r0/LP7vxXmhNR9w6S3qb8w8YFfqZlyMB+tYeWof+LgYPs+lk3BzqIfhjDk8zNUAFbM4rTPtEOCGwH0IayGISuqGlWivnC1czsEXNNUZVogncM9llAGjisPBbepLFxUF1z9R3KM0nCdpjaD89WP8rNbamEfk2D49ab0V5mbUNfZiKF44jWk9AK361V9vxY99Yrvg0ScPxhFzIKsjPRKlkZLAM24jhI6gciX0Nx/7o4Udq/LsZ7K/94Hs6SvfVoBfl8U4jzwsWIYYkdOSwsiBU3Ujfy5lqZ1X1izH8IioqLZLw976dyg4d3KXCyVHvpKGBE6IFb/h8XhaBKhTxespEvFBVXADkPKIbDAvG3cQ/9A8TuZX2aH/dYtl7xSO7OM23xslpyTQnb1nQUw7aOQyUFAfFaIERkOlvbQFQXGjZK2foXD7RxoVwqFvdQdatyN12lVhPuDoowQyNnKSyMAae8w2pfGk3DFfr/yOy20P7+z6gOsyLUlOQqPuYVoa3rmytIvEdMFtXil8RdsnUF/bmOsXusM1HYSvMwepKUwyLFqM7bPemz6ra2fZyFeKFQ3DP1wTbgsD/W3/O/KTNfib9G9LmQb3DYQzF6ULxRhC+h42fnfPJv+IIz/KyBlrt7MABYAXunV/rtgVMTwTl4DF0HrZfrdxaTQ565ywhP7oKbzw296nyWlMck+wV+y20cwkaE4TRxOifpVjfIAzusNPEK82N6Kqr0M33XtGeHy3x4yN+uxKzFx+7cxLYl0Udg9eFazecHfy/JP+o0rK2HdvcnUqvTZyTw0LXDhs9wmKSaC3/lN2w5xfVV3aIvz4RWS00cn2SIK1iEnFV/KphoDIy/Kr0rga6DhstQsLAOSgnqrp+1RXwC3z5RL3kRxy7G0FaW2gTyf6IzM5NQEEH5etKlFIfne5wbDG/GTA+ZOc+gpj1xRNr4l9cieTVj+d13c3W7OABUKjxfHsvLCWsWR8kBdwTJRI7OlMkzaVJk7I3y39azhwbNplCFXNMEffzBOQ3TYc/H54ECR/TdqR26xJxEAp+I+h1Tn8QNNT1wYknnCOvQ5DAX1QYqpp0dzqfwlKqpmK7hRNquii1ly7bXnGvLkHAcAuNfdfLX6sLTyW9kWbbaaDkbOVRlkX3xRiT/a1wljNIEesckpxMjXGKLBE8KwfkGuJhb8URMqMbGqLeh6FePCMOaWfaZnTzDft3pHzXoepKletq31PX8+LqK+zk46y1rA67Zfxol96RsGZaC9mWM4kglCa7IJKnH4shM8x9l9Bo+eSUmhrVfH2VoLa0De435SLgJIZVJ0jaJYxUNwFEC0cGJTH63Fr2piz97rSftUcLbCM4K+qDkOJDpdm1KknV49rNh1S1N0Ztzo+ob/mKUwjnuS6XHMC7AjfFHsRltd+ghs18lbM/yRTMJZz68eCsIu3lRzmGZMgbeKFdgT8jq1LVxPpOCA0ofR+V1AHzujxfq0YNT2PH/S1RUgck2HOOpWqdouJN55Vxt9sLMIer3hIvi28swXtIhWUES+DG7EJfO22zMhaGjeg2D8VpNPwU++/zNmSbM2sWRgWC17N1gln5yQfqVqZtGQ6waKFAJH3Bjc9UJMRl+zZ95hNxoifG+TRVrXHRToXuwQHcxi+fBjpnBqPTuHK9AYsRO1BZiQLTu1cB0koo7RkbHEW7TuAke4S90qX3UGk450rksdaYwpeTSsEmDBS7a4ptPHqbTA8m4lGTOFNzzO7yjRjleqjpjWDakamXjNo9YWtJfKWYlMPIcsYvDoQ3GdVQtzZK2xOykLs40JpT7ZSwrteXlh2ZFsxalUQqf9ZO1car34R9QuioG2CxK0tC48stnjhyEdmpwbiQeg6CkgY+VZ2b7+dd22PE1iSt2IZ2GsFPdz9yumftzdtDxNzrQAeecDm37cVucJpbcjRjGbsqdBjsEGcMLAVKF9t8GS7Np65Tj5Iqdqq3Ww+Ui8izoc7nXyy+3IODza3UBM0ecZeFhK8GQFlSy0Ubg1b/3zxJ2JVys2aEDrc8dykWo1wlItVrg7keYjolAnXlPhZEXj/noSOg3DF5tTSYjve8qDxCae7Vpz7DbHAa84lsAplnjbsm7tkz3CcUvKK17cryCT99X4pKsNj1MDflQ0kYRBqRJw5LYD49+JOBXUA7m3ybFd2diH7YPbr4AoFXOl/5r5B+Up+KQ9pVvLZNArQ7NJCI7SZidK62CX7KaneDv8N/uZxQ48CoBeHs4Xm5xnnkulgrT2R/C/1WVoAf5YSTVtKaKkk9YHHF5crkdtyX65d8BBtA11FR8nVP9KxQQR6D7OJkl7Q/1RMgiXz3CV6mFovyI=";
    private ChirpSDK chirp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_layout);
        getSupportActionBar().setElevation(0);


        chirp = new ChirpSDK(this, CHIRP_APP_KEY, CHIRP_APP_SECRET);
        Log.v("CHIRP", "ChirpSDK Version: " + chirp.getVersion());

        ChirpError setConfigError = chirp.setConfig(CHIRP_APP_CONFIG);
        if (setConfigError.getCode() > 0) {
            Log.e("CHIRP", setConfigError.getMessage());
            return;
        }
        chirp.setListener(new ChirpEventListener() {
            @Override
            public void onSent(@NotNull byte[] bytes, int i) {

            }

            @Override
            public void onSending(@NotNull byte[] bytes, int i) {

            }

            @Override
            public void onReceived(@Nullable byte[] bytes, int i) {
                if (bytes != null) {
                    String identifier = new String(bytes);
                    Log.v("ChirpSDK: ", "Received " + identifier);
                } else {
                    Log.e("ChirpError: ", "Decode failed");
                }
            }

            @Override
            public void onReceiving(int i) {

            }

            @Override
            public void onStateChanged(int i, int i1) {

            }

            @Override
            public void onSystemVolumeChanged(float v, float v1) {

            }
        });


        doneText = findViewById(R.id.textView3);
        iconDone = findViewById(R.id.doneView);
        doneButton = findViewById(R.id.doneButton);

        payCodeTX = findViewById(R.id.textView4);
        payCodeTV = findViewById(R.id.payCodeTV);
        payCodeTV.setText(getIntent().getStringExtra("code"));
        progressBar = findViewById(R.id.progressBar2);

        sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", getIntent().getStringExtra("psbQR"));
        editor.apply();

        dtmf = findViewById(R.id.textView);
        ultrasonic = findViewById(R.id.textView2);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isCheckSend) {
                    isCheckSend = true;
                    restTemplate.checkTransaction(userToken, getIntent().getStringExtra("transactionId"))
                            .enqueue(new Callback<JsonObject>() {
                                @Override
                                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                    isCheckSend = false;
                                    if (response.body() != null)
                                        if (response.body().get("status").getAsInt() == 2) {
                                            showDone();

                                            frameSubscription.unsubscribe();
                                        }
                                }

                                @Override
                                public void onFailure(Call<JsonObject> call, Throwable t) {
                                    isCheckSend = false;
                                    System.err.println(t);
                                }
                            });
                }
            }
        }, 0, 500);

        doneButton.setOnClickListener(v -> {
            timer.cancel();
            timer.purge();

            frameSubscription.unsubscribe();
            ReceiveActivity.this.finish();
        });
        setupReceiver();

        buttonInit = findViewById(R.id.buttonDecline);
        buttonInit.setOnClickListener(view -> {
            timer.cancel();
            timer.purge();

            frameSubscription.unsubscribe();
            transactionDecline();
            ReceiveActivity.this.finish();
        });
        qrView = findViewById(R.id.qrView);
        sound = findViewById(R.id.soundType);
        sound.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //frameSubscription.unsubscribe();
                //startSdk();
            } else {
                //stopSdk();
                //setupReceiver();
            }
        });


        QRGEncoder qrgEncoder = new QRGEncoder(String.format("https://terminalpay.zingpay.ru/?tid=%s", getIntent().getStringExtra("transactionId")), null, QRGContents.Type.TEXT, 950);

        Bitmap bitmap;
        try {
            bitmap = qrgEncoder.encodeAsBitmap();
            qrView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        final boolean[] isChecked = new boolean[]{true};
        TextView qrType = findViewById(R.id.textView4);
        qrView.setOnClickListener(v -> {
            if (!isChecked[0]) {
                qrType.setText("Оплата картой");
                isChecked[0] = true;
                QRGEncoder qrgEncoder1 = new QRGEncoder(String.format("https://terminalpay.zingpay.ru/?tid=%s", getIntent().getStringExtra("transactionId")), null, QRGContents.Type.TEXT, 950);

                Bitmap bitmap1;
                try {
                    bitmap1 = qrgEncoder1.encodeAsBitmap();
                    qrView.setImageBitmap(bitmap1);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            } else {
                qrType.setText("Оплата СБП");
                isChecked[0] = false;
                QRGEncoder qrgEncoder1 = new QRGEncoder(getIntent().getStringExtra("psbQR"), null, QRGContents.Type.TEXT, 950);

                Bitmap bitmap1;
                try {
                    bitmap1 = qrgEncoder1.encodeAsBitmap();
                    qrView.setImageBitmap(bitmap1);
                } catch (WriterException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void stopSdk() {
        if (chirp == null) return;
        ChirpError error = chirp.stop();
        if (error.getCode() > 0) {
            Log.e("CHIRP", error.getMessage());
            return;
        }
    }

    public void startSdk() {
        if (chirp == null) return;
        ChirpError error = chirp.start();
        if (error.getCode() > 0) {
            Log.e("CHIRP", error.getMessage());
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ultrasonicAccepted = false;
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
        try {
            chirp.stop();
            chirp.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("psbQR", "");
        editor.apply();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    subscribeToFrames();
                } else {
                    showMissingAudioPermissionToast();
                    finish();
                }
            }
        }
    }

    private void setupReceiver() {
        if (hasRecordAudioPermission()) {
            subscribeToFrames();
        } else {
            requestPermission();
        }
    }

    private boolean ultrasonicAccepted = false;

    public void transactionDecline() {
        restTemplate.declineTransaction(userToken, getIntent().getStringExtra("transactionId")).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                System.err.println(call);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("psbQR", "");
                editor.apply();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void subscribeToFrames() {
        ultrasonicAccepted = false;
        frameSubscription.unsubscribe();
        frameSubscription = FrameReceiverObservable
                .create(this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bytes -> {
                    Log.e("TRANSA", new String(bytes, Charset.forName("UTF-8")) + ultrasonicAccepted);
                    if (!ultrasonicAccepted) {
                        ultrasonicAccepted = true;
                        String transactionString = new String(bytes, Charset.forName("UTF-8"));

                        final String substring = transactionString.substring(1, transactionString.length() - 1);
                        System.err.println(substring);
                        progressBar.setVisibility(View.VISIBLE);
                        restTemplate.receivePay(userToken,
                                substring).enqueue(new Callback<PayResponse>() {
                            @Override
                            public void onResponse(Call<PayResponse> call, Response<PayResponse> response) {
                                if (response.body() != null) {
                                    Log.e("test", String.valueOf(response.body()));
                                    progressBar.setVisibility(View.INVISIBLE);
                                    if (response.body().getStatus() != null) {
                                        if (response.body().getStatus().equals("OK")) {
                                            showDone();


                                        }
                                    } else {

                                        Toast.makeText(ReceiveActivity.this, String.valueOf(response.body()), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<PayResponse> call, Throwable t) {
                                t.fillInStackTrace();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                });
    }

    private boolean hasRecordAudioPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
    }

    private void showMissingAudioPermissionToast() {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, R.string.missing_audio_permission, duration);
        toast.show();
    }

    private void showDone() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }

        frameSubscription.unsubscribe();


        payCodeTX.setVisibility(View.INVISIBLE);
        payCodeTV.setVisibility(View.INVISIBLE);
        dtmf.setVisibility(View.INVISIBLE);
        ultrasonic.setVisibility(View.INVISIBLE);
        qrView.setVisibility(View.INVISIBLE);
        buttonInit.setVisibility(View.INVISIBLE);
        sound.setVisibility(View.INVISIBLE);

        doneText.setVisibility(View.VISIBLE);
        doneButton.setVisibility(View.VISIBLE);
        iconDone.setVisibility(View.VISIBLE);
    }
}
