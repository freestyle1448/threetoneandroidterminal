package com.example.terminal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.ncorti.slidetoact.SlideToActView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.terminal.models.Constants.APP_PREFERENCES;
import static com.example.terminal.models.Constants.TERMINAL_TOKEN;

public class MainActivity extends AppCompatActivity {
    private final RestTemplate restTemplate = new RestTemplate();
    private EditText amountEditText;
    private String userToken;
    private ProgressBar progressBar;
    private SlideToActView buttonCreate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.init_layout);
        getSupportActionBar().setElevation(0);
        progressBar = findViewById(R.id.progressBar);
        SharedPreferences sharedPreferences = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        userToken = sharedPreferences.getString(TERMINAL_TOKEN, "");

        findViewById(R.id.exitButton).setOnClickListener(v -> {
            SharedPreferences preferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.apply();

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        });

        buttonCreate = findViewById(R.id.buttonDecline);
        buttonCreate.setText("");
        amountEditText = findViewById(R.id.amountEditText);
        amountEditText.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                createInitRequest();
                handled = true;
            }
            return handled;
        });

        buttonCreate.setOnSlideCompleteListener(view -> {
            progressBar.setVisibility(View.VISIBLE);
            buttonCreate.setClickable(false);
            createInitRequest();
            buttonCreate.resetSlider();
        });
    }

    public void createInitRequest() {

        System.out.println(amountEditText.getText().length());
        if (amountEditText.getText().length() > 0) {
            final String amount = amountEditText.getText().toString();
            restTemplate.createTransaction(userToken, (long) (Double.valueOf(amount) * 100))
                    .enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.body() != null) {
                                final String transactionId = response.body().get("transactionId").getAsString();
                                if (transactionId != null) {
                                    Intent intent = new Intent(MainActivity.this, ReceiveActivity.class);
                                    intent.putExtra("psbQR", response.body().get("psb").getAsJsonObject().get("qr_DATA").getAsString());
                                    intent.putExtra("transactionId", transactionId);
                                    intent.putExtra("amount", Double.valueOf(amount));
                                    intent.putExtra("code", response.body().get("code").getAsString());
                                    startActivity(intent);

                                    progressBar.setVisibility(View.INVISIBLE);
                                    amountEditText.setText("");

                                }
                            } else {
                                Toast.makeText(MainActivity.this, "Произошла ошибка, повторите запрос позже.\nОшибка - "
                                        + "Не удалось создать транзакцию!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            System.err.println(t);
                            buttonCreate.setClickable(true);
                        }
                    });
        } else {
            progressBar.setVisibility(View.INVISIBLE);
            amountEditText.requestFocus();
            amountEditText.setError("Введите сумму!");
            buttonCreate.setClickable(true);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        buttonCreate.setClickable(true);
    }
}
